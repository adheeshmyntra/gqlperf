
import {
  GraphQLBoolean,
  GraphQLString,
  GraphQLInt,
  GraphQLFloat,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLID,
  GraphQLNonNull,
  GraphQLList
} from 'graphql';

import data from './data';
import data2 from './data2';

const OWNERType = new GraphQLObjectType({
  name: 'OWNERType',
  fields: () => ({
    login: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for login
      //resolve: () => null,
    },
    id: {
      description: 'enter your description',
      type: GraphQLInt,
      // TODO: Implement resolver for id
      //resolve: () => null,
    },
    avatar_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for avatar_url
      //resolve: () => null,
    },
    gravatar_id: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for gravatar_id
      //resolve: () => null,
    },
    url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for url
      //resolve: () => null,
    },
    html_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for html_url
      //resolve: () => null,
    },
    followers_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for followers_url
      //resolve: () => null,
    },
    following_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for following_url
      //resolve: () => null,
    },
    gists_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for gists_url
      //resolve: () => null,
    },
    starred_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for starred_url
      //resolve: () => null,
    },
    subscriptions_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for subscriptions_url
      //resolve: () => null,
    },
    organizations_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for organizations_url
      //resolve: () => null,
    },
    repos_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for repos_url
      //resolve: () => null,
    },
    events_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for events_url
      //resolve: () => null,
    },
    received_events_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for received_events_url
      //resolve: () => null,
    },
    type: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for type
      //resolve: () => null,
    },
    site_admin: {
      description: 'enter your description',
      type: GraphQLBoolean,
      // TODO: Implement resolver for site_admin
      //resolve: () => null,
    }
  })
});

const ReposType = new GraphQLObjectType({
  name: 'ReposType',
  fields: () => ({
    id: {
      description: 'enter your description',
      type: GraphQLInt,
      // TODO: Implement resolver for id
      //resolve: () => null,
    },
    name: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for name
      //resolve: () => null,
    },
    full_name: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for full_name
      //resolve: () => null,
    },
    owner: {
      description: 'enter your description',
      type: OWNERType,
      // TODO: Implement resolver for owner
      //resolve: () => null,
    },
    private: {
      description: 'enter your description',
      type: GraphQLBoolean,
      // TODO: Implement resolver for private
      //resolve: () => null,
    },
    html_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for html_url
      //resolve: () => null,
    },
    description: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for description
      //resolve: () => null,
    },
    fork: {
      description: 'enter your description',
      type: GraphQLBoolean,
      // TODO: Implement resolver for fork
      //resolve: () => null,
    },
    url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for url
      //resolve: () => null,
    },
    forks_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for forks_url
      //resolve: () => null,
    },
    keys_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for keys_url
      //resolve: () => null,
    },
    collaborators_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for collaborators_url
      //resolve: () => null,
    },
    teams_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for teams_url
      //resolve: () => null,
    },
    hooks_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for hooks_url
      //resolve: () => null,
    },
    issue_events_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for issue_events_url
      //resolve: () => null,
    },
    events_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for events_url
      //resolve: () => null,
    },
    assignees_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for assignees_url
      //resolve: () => null,
    },
    branches_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for branches_url
      //resolve: () => null,
    },
    tags_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for tags_url
      //resolve: () => null,
    },
    blobs_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for blobs_url
      //resolve: () => null,
    },
    git_tags_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for git_tags_url
      //resolve: () => null,
    },
    git_refs_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for git_refs_url
      //resolve: () => null,
    },
    trees_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for trees_url
      //resolve: () => null,
    },
    statuses_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for statuses_url
      //resolve: () => null,
    },
    languages_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for languages_url
      //resolve: () => null,
    },
    stargazers_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for stargazers_url
      //resolve: () => null,
    },
    contributors_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for contributors_url
      //resolve: () => null,
    },
    subscribers_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for subscribers_url
      //resolve: () => null,
    },
    subscription_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for subscription_url
      //resolve: () => null,
    },
    commits_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for commits_url
      //resolve: () => null,
    },
    git_commits_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for git_commits_url
      //resolve: () => null,
    },
    comments_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for comments_url
      //resolve: () => null,
    },
    issue_comment_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for issue_comment_url
      //resolve: () => null,
    },
    contents_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for contents_url
      //resolve: () => null,
    },
    compare_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for compare_url
      //resolve: () => null,
    },
    merges_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for merges_url
      //resolve: () => null,
    },
    archive_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for archive_url
      //resolve: () => null,
    },
    downloads_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for downloads_url
      //resolve: () => null,
    },
    issues_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for issues_url
      //resolve: () => null,
    },
    pulls_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for pulls_url
      //resolve: () => null,
    },
    milestones_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for milestones_url
      //resolve: () => null,
    },
    notifications_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for notifications_url
      //resolve: () => null,
    },
    labels_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for labels_url
      //resolve: () => null,
    },
    releases_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for releases_url
      //resolve: () => null,
    },
    deployments_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for deployments_url
      //resolve: () => null,
    },
    created_at: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for created_at
      //resolve: () => null,
    },
    updated_at: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for updated_at
      //resolve: () => null,
    },
    pushed_at: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for pushed_at
      //resolve: () => null,
    },
    git_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for git_url
      //resolve: () => null,
    },
    ssh_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for ssh_url
      //resolve: () => null,
    },
    clone_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for clone_url
      //resolve: () => null,
    },
    svn_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for svn_url
      //resolve: () => null,
    },
    homepage: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for homepage
      //resolve: () => null,
    },
    size: {
      description: 'enter your description',
      type: GraphQLInt,
      // TODO: Implement resolver for size
      //resolve: () => null,
    },
    stargazers_count: {
      description: 'enter your description',
      type: GraphQLInt,
      // TODO: Implement resolver for stargazers_count
      //resolve: () => null,
    },
    watchers_count: {
      description: 'enter your description',
      type: GraphQLInt,
      // TODO: Implement resolver for watchers_count
      //resolve: () => null,
    },
    language: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for language
      //resolve: () => null,
    },
    has_issues: {
      description: 'enter your description',
      type: GraphQLBoolean,
      // TODO: Implement resolver for has_issues
      //resolve: () => null,
    },
    has_downloads: {
      description: 'enter your description',
      type: GraphQLBoolean,
      // TODO: Implement resolver for has_downloads
      //resolve: () => null,
    },
    has_wiki: {
      description: 'enter your description',
      type: GraphQLBoolean,
      // TODO: Implement resolver for has_wiki
      //resolve: () => null,
    },
    has_pages: {
      description: 'enter your description',
      type: GraphQLBoolean,
      // TODO: Implement resolver for has_pages
      //resolve: () => null,
    },
    forks_count: {
      description: 'enter your description',
      type: GraphQLInt,
      // TODO: Implement resolver for forks_count
      //resolve: () => null,
    },
    mirror_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for mirror_url
      //resolve: () => null,
    },
    open_issues_count: {
      description: 'enter your description',
      type: GraphQLInt,
      // TODO: Implement resolver for open_issues_count
      //resolve: () => null,
    },
    forks: {
      description: 'enter your description',
      type: GraphQLInt,
      // TODO: Implement resolver for forks
      //resolve: () => null,
    },
    open_issues: {
      description: 'enter your description',
      type: GraphQLInt,
      // TODO: Implement resolver for open_issues
      //resolve: () => null,
    },
    watchers: {
      description: 'enter your description',
      type: GraphQLInt,
      // TODO: Implement resolver for watchers
      //resolve: () => null,
    },
    default_branch: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for default_branch
      //resolve: () => null,
    }
  })
});

const UserType = new GraphQLObjectType({
  name:'UserType',
  description: '...',

  fields: () => ({
    login: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for login
      //resolve: () => null,
    },
    id: {
      description: 'enter your description',
      type: GraphQLInt,
      // TODO: Implement resolver for id
      //resolve: () => null,
    },
    avatar_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for avatar_url
      //resolve: () => null,
    },
    gravatar_id: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for gravatar_id
      //resolve: () => null,
    },
    url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for url
      //resolve: () => null,
    },
    html_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for html_url
      //resolve: () => null,
    },
    followersUrl: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for followers_url
      resolve: (user) => user.followers_url,
    },
    following_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for following_url
      //resolve: () => null,
    },
    gists_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for gists_url
      //resolve: () => null,
    },
    starred_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for starred_url
      //resolve: () => null,
    },
    subscriptions_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for subscriptions_url
      //resolve: () => null,
    },
    organizations_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for organizations_url
      //resolve: () => null,
    },
    repos_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for repos_url
      //resolve: () => null,
    },
    events_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for events_url
      //resolve: () => null,
    },
    received_events_url: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for received_events_url
      //resolve: () => null,
    },
    type: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for type
      //resolve: () => null,
    },
    site_admin: {
      description: 'enter your description',
      type: GraphQLBoolean,
      // TODO: Implement resolver for site_admin
      //resolve: () => null,
    },
    name: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for name
      //resolve: () => null,
    },
    company: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for company
      //resolve: () => null,
    },
    blog: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for blog
      //resolve: () => null,
    },
    location: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for location
      //resolve: () => null,
    },
    email: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for email
      //resolve: () => null,
    },
    hireable: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for hireable
      //resolve: () => null,
    },
    bio: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for bio
      //resolve: () => null,
    },
    public_repos: {
      description: 'enter your description',
      type: GraphQLInt,
      // TODO: Implement resolver for public_repos
      //resolve: () => null,
    },
    public_gists: {
      description: 'enter your description',
      type: GraphQLInt,
      // TODO: Implement resolver for public_gists
      //resolve: () => null,
    },
    followers: {
      description: 'enter your description',
      type: GraphQLInt,
      // TODO: Implement resolver for followers
      //resolve: () => null,
    },
    following: {
      description: 'enter your description',
      type: GraphQLInt,
      // TODO: Implement resolver for following
      //resolve: () => null,
    },
    repos: {
      description: 'enter your description',
      type: new GraphQLList(ReposType),
      // TODO: Implement resolver for following
      resolve: (user) => data2[user.login],
    },
    created_at: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for created_at
      //resolve: () => null,
    },
    updated_at: {
      description: 'enter your description',
      type: GraphQLString,
      // TODO: Implement resolver for updated_at
      //resolve: () => null,
    }
  })
});

const QueryType = new GraphQLObjectType({
  name: 'Query',
  description: '...',

  fields: () => ({
    user: {
      type: new GraphQLList(UserType),
      args: {
        login: {type: GraphQLString}
      },
      resolve: (root, args) => data
    }
  })
});

export default new GraphQLSchema({
  query: QueryType,
});
