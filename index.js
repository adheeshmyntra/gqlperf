import express from 'express';
import graphQLHTTP from 'express-graphql';
import {graphql} from 'graphql';

import schema from './schema';
import schema2 from './schema2';
import data from './data';
import data2 from './data2';

import async from 'async'

var fs = require('fs');

const graphqlServer = express();
const app = express();

const query = `{
  user{
    login
    id
    avatar_url
    gravatar_id
    url
    html_url
    followersUrl
    gists_url
    starred_url
    subscriptions_url
    organizations_url
    repos_url
    events_url
    received_events_url
    type
    site_admin
    name
    company
    blog
    location
    email
    hireable
    bio
    public_repos
    public_gists
    followers
    following
    created_at
    updated_at
 }
}`

const query2 = `{
  user{
    login
    id
    avatar_url
    gravatar_id
    url
    html_url
    followersUrl
    following_url
    gists_url
    starred_url
    subscriptions_url
    organizations_url
    repos_url
    events_url
    received_events_url
    type
    site_admin
    name
    company
    blog
    location
    email
    hireable
    bio
    public_repos
    public_gists
    followers
    following
    created_at
    updated_at
    repos {
      id
      name
    }
 }
}`;

const query3 = `{
  user{
    login
    id
    avatar_url
    gravatar_id
    url
    html_url
    followersUrl:following_url
    gists_url
    starred_url
    subscriptions_url
    organizations_url
    repos_url
    events_url
    received_events_url
    type
    site_admin
    name
    company
    blog
    location
    email
    hireable
    bio
    public_repos
    public_gists
    followers
    following
    created_at
    updated_at
 }
}`;
Object.prototype.renameProperty = function (oldName, newName) {
     if (oldName == newName) {
         return this;
     }
    if (this.hasOwnProperty(oldName)) {
        this[newName] = this[oldName];
        delete this[oldName];
    }
    return this;
};

graphqlServer.use(graphQLHTTP({
	schema,
  graphiql: true
}));

// app.get('/', (req,res) => {
// 	data.renameProperty('followers_url', 'followersUrl');
// 	res.setHeader('Content-Type', 'application/json');
// 	res.setHeader('Cache-Control', 'no-cache');
// 	for(let i=0; i<data.length; i++) {
//   		data[i].renameProperty('followers_url', 'followersUrl');
//   	}
// 	res.json(data);
// });

// app.get('/graphql', (req, res) => {
// 	graphql(schema, query).then(result => {
//   		res.json(result);
// 	});
// });

app.get('/comparison', (req, res) => {
	const now  = new Date();
	graphql(schema, query).then(result => {
		let then = new Date();
  		console.log(then - now);
  		then = new Date();
  		for(let i=0; i<data.length; i++) {
  			data[i].renameProperty('followers_url', 'followersUrl');
  		}
		let then2 = new Date();
		console.log(then2 - then);
		res.json(result);
	});
});


app.get('/nestcomparison', (req, res) => {
  const now  = new Date();
  graphql(schema, query2).then(result => {
    let then = new Date();
    console.log(then - now);
    then = new Date();
    for(let i=0; i<data.length; i++) {
      data[i].renameProperty('followers_url', 'followersUrl');
      let login = data[i].login;
      let repos = data2[login];
      data['repos'] = [];
      repos.forEach((repo) => {
        let repoData = Object.assign({},{id: repo.id, name: repo.name});
        data['repos'].push(repoData);
      });
    }
    let then2 = new Date();
    console.log(then2 - then);
    res.json(result);
  });
});

app.get('/keyinquery', (req, res) => {
  const now  = new Date();
  graphql(schema2, query3).then(result => {
    let then = new Date();
      console.log(then - now);
      then = new Date();
      for(let i=0; i<data.length; i++) {
        data[i].renameProperty('followers_url', 'followersUrl');
      }
    let then2 = new Date();
    console.log(then2 - then);
    res.json(result);
  });
});

var doQuery = function (callback) {
  const now  = new Date();
  graphql(schema2, query3).then(result => {
    let then = new Date();
    callback(null, then-now);
  });
}

app.get('/multiple', (req, res) => {
  async.timesSeries(10000, (n, next) => {
    doQuery( (err, result) => {
      console.log(n);
      next(err, result);
    });
  }, (error, results) => {
      console.log(results);
      fs.writeFile(__dirname+"/test.json", results, (err) => {
      if(err) {
          return console.log(err);
      }
      console.log("Done");
      });
      res.json(results[0]);
    });
});

graphqlServer.listen(5000);
app.listen(5001);